import {
  ActionsApi,
  AlfrescoApi,
  AspectsApi,
  CategoriesApi,
  CommentsApi,
  ContentApi,
  DownloadsApi,
  FavoritesApi,
  GroupsApi,
  NodesApi,
  PeopleApi,
  RenditionsApi,
  SearchApi,
  SharedlinksApi,
  SitesApi,
  TagsApi,
  TrashcanApi,
  TypesApi,
  UploadApi,
  VersionsApi,
  WebscriptApi,
} from "@alfresco/js-api";

export const alfrescoApi = new AlfrescoApi();

export const contentApi = new ContentApi(alfrescoApi);

export const uploadApi = new UploadApi(alfrescoApi);

export const renditionsApi = new RenditionsApi(alfrescoApi);

export const nodesApi = new NodesApi(alfrescoApi);

export const sitesApi = new SitesApi(alfrescoApi);

export const searchApi = new SearchApi(alfrescoApi);

export const peopleApi = new PeopleApi(alfrescoApi);

export const favoritesApi = new FavoritesApi(alfrescoApi);

export const trashcanApi = new TrashcanApi(alfrescoApi);

export const groupsApi = new GroupsApi(alfrescoApi);

export const sharedLinksApi = new SharedlinksApi(alfrescoApi);

export const versionsApi = new VersionsApi(alfrescoApi);

export const actionsApi = new ActionsApi(alfrescoApi);

export const categoriesApi = new CategoriesApi(alfrescoApi);

export const commentsApi = new CommentsApi(alfrescoApi);

export const downloadsApi = new DownloadsApi(alfrescoApi);

export const tagsApi = new TagsApi(alfrescoApi);

export const aspectsApi = new AspectsApi(alfrescoApi);

export const typesApi = new TypesApi(alfrescoApi);

export const webscriptApi = new WebscriptApi(alfrescoApi);
