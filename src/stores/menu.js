/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";

export const useMenuStore = defineStore({
  id: "MenuStore",
  state: () => ({
    menu: [],
  }),
  actions: {
    addFavorite(item, targetMenu) {
      if (!item) {
        console.warn("addFavorite, item is undefined");
        return;
      }

      this.menu.find((element) => element.key === targetMenu).items.push(item);
    },
    removeFavorite(item, targetMenu) {
      if (!item) {
        console.warn("removeFavorite, item is undefined");
        return;
      }

      let index = this.menu
        .find((element) => element.key === targetMenu)
        .items.findIndex((targetMenuItem) =>
          "FolderId" in targetMenuItem.to.params
            ? targetMenuItem.to.params.FolderId === item.to.params.FolderId
            : targetMenuItem.to.params.id === item.to.params.id,
        );
      this.menu
        .find((element) => element.key === targetMenu)
        .items.splice(index, 1);
    },
  },
});
