/*
  Copyright (C) 2022 - Jeci SARL - https://jeci.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";
import alfrescoNodeService from "../AlfrescoNodeService";
import workspaceService from "../WorkspaceService";

/**
 * Store current folder, workspace and child nodes
 * @type {StoreDefinition<string, {workspace: null, nodes: [], folders: [], role: null, children: [], emptyFolder: boolean, currentNode: null, files: [], workspaceInfo: null, loading: boolean, canUpload: boolean}, {}, {selectNodes(*): void, load(*, *): void, selectFiles(*): void, selectFolders(*): void}>}
 */
export const useNavigationStore = defineStore({
  id: "NavigationStore",
  state: () => ({
    currentNode: null,
    totalRecordNodes: null,
    workspace: null,
    workspaceInfo: null,
    nodes: [],
    files: [],
    folders: [],
    children: [],
    loading: true,
    emptyFolder: false,
    role: null,
    canUpload: false,
    pagination: null,
    isInConsultation: false,
    isInUserHome: false,
    hasNoUserHome: false,
  }),
  actions: {
    /**
     * Load children from current node (if it change)
     * @param workspaceId Current Workspace id (mandatory)
     * @param folderId Current Folder id (optional)
     * @param force force load (reload) data
     * @return Promise
     */
    load(workspaceId, folderId, force) {
      if (
        !force &&
        workspaceId === this.workspace?.id &&
        folderId === this.currentNode?.id
      ) {
        // Skip
        return Promise.resolve();
      }

      this.loading = true;
      this.emptyFolder = false;
      this.pagination = null;
      return workspaceService
        .getWorkspace(workspaceId)
        .then((workspace) => {
          this.workspace = workspace;
          this.role = workspace.role;
          this.visibility = workspace.visibility;
          if (folderId) {
            return alfrescoNodeService.getNode(folderId).then((currentNode) => {
              return currentNode;
            });
          } else {
            return alfrescoNodeService
              .getNode(workspace.guid)
              .then((currentSpace) => {
                this.workspaceInfo = currentSpace;
                return currentSpace;
              });
          }
        })
        .then((currentNode) => {
          this.canUpload =
            currentNode.allowableOperations?.includes("create") || false;

          return alfrescoNodeService
            .getChildren(currentNode.guid || currentNode.id)
            .then((response) => {
              if (response.documentLibrary) {
                this.currentNode = response.documentLibrary;
              } else {
                this.currentNode = currentNode;
              }
              this.canUpload =
                this.currentNode.allowableOperations?.includes("create") ||
                false;
              this.children = response.children;
              this.pagination = response.pagination;

              if (this.children.length > 0) {
                this.folders = this.children.filter((child) => child.isFolder);
                this.files = this.children.filter((child) => child.isFile);
              } else {
                this.folders = [];
                this.files = [];
                this.emptyFolder = true;
              }
              this.loading = false;

              return currentNode;
            });
        });
    },
    loadWithOpts(workspaceId, folderId, force, opts) {
      if (
        !force &&
        workspaceId === this.workspace?.id &&
        folderId === this.currentNode?.id
      ) {
        // Skip
        return Promise.resolve();
      }
      this.loading = true;
      this.emptyFolder = false;
      this.pagination = null;

      let workspacePromise;
      if (workspaceId) {
        this.isInUserHome = false;
        workspacePromise = workspaceService.getWorkspace(workspaceId);
      } else {
        workspacePromise = alfrescoNodeService
          .getUserHomeNode()
          .then((userHomeNode) => {
            this.isInUserHome = true;
            return userHomeNode;
          });
      }
      return workspacePromise
        .then((workspace) => {
          this.workspace = workspace;
          this.role = workspace.role;
          this.visibility = workspace.visibility;
          if (folderId) {
            return alfrescoNodeService.getNode(folderId).then((currentNode) => {
              return currentNode;
            });
          } else {
            return alfrescoNodeService
              .getNode(workspace.guid)
              .then((currentSpace) => {
                this.workspaceInfo = currentSpace;
                return currentSpace;
              });
          }
        })
        .then((currentNode) => {
          this.canUpload =
            currentNode.allowableOperations?.includes("create") || false;

          return alfrescoNodeService
            .getChildrenWithOpts(currentNode.guid || currentNode.id, opts)
            .then((response) => {
              this.totalRecordNodes = response.pagination.totalItems;
              if (response.documentLibrary) {
                this.currentNode = response.documentLibrary;
              } else {
                this.currentNode = currentNode;
              }
              this.canUpload =
                this.currentNode.allowableOperations?.includes("create") ||
                false;
              this.children = response.children;
              this.pagination = response.pagination;

              if (this.children.length > 0) {
                this.folders = this.children.filter((child) => child.isFolder);
                this.files = this.children.filter((child) => child.isFile);
              } else {
                this.folders = [];
                this.files = [];
                this.emptyFolder = true;
              }
              this.loading = false;

              return currentNode;
            });
        });
    },

    /**
     * Load children from current node in Actes Application (if it change)
     * @param workspaceId Current Workspace id (mandatory)
     * @param seanceId Current seance id (optional)
     * @param force force load (reload) data
     * @return Promise
     */
    loadOnActes(workspaceId, seanceId, force) {
      if (
        !force &&
        workspaceId === this.workspace?.id &&
        seanceId === this.currentNode?.id
      ) {
        // Skip
        return Promise.resolve();
      }

      this.loading = true;
      this.emptyFolder = false;
      this.pagination = null;
      return workspaceService
        .getWorkspace(workspaceId)
        .then((workspace) => {
          this.workspace = workspace;
          this.role = workspace.role;
          this.visibility = workspace.visibility;
          if (seanceId) {
            return alfrescoNodeService.getNode(seanceId).then((currentNode) => {
              return currentNode;
            });
          } else {
            return alfrescoNodeService
              .getNode(workspace.guid)
              .then((currentSpace) => {
                this.workspaceInfo = currentSpace;
                return currentSpace;
              });
          }
        })
        .then((currentNode) => {
          this.canUpload =
            currentNode.allowableOperations?.includes("create") || false;

          return alfrescoNodeService
            .getChildren(currentNode.guid || currentNode.id)
            .then((response) => {
              if (response.documentLibrary) {
                alfrescoNodeService
                  .getChildren(response.documentLibrary.id)
                  .then(async (yearFolder) => {
                    const monthChildrenArrays = [];

                    for (const yearChild of yearFolder.children) {
                      const monthFolder = await alfrescoNodeService.getChildren(
                        yearChild.id,
                      );
                      const folderPromises = monthFolder.children
                        .filter((item) => item.nodeType === "cm:folder")
                        .map((monthChild) =>
                          alfrescoNodeService.getChildren(monthChild.id),
                        );

                      const monthChildren = await Promise.all(folderPromises);
                      monthChildrenArrays.push(monthChildren);
                    }

                    const allChildren = monthChildrenArrays.flat(2);
                    const allMonthChildren = allChildren.flat();
                    this.files = allMonthChildren.flatMap(
                      (monthChild) => monthChild.children,
                    );
                    this.pagination = {
                      count: this.files.length,
                      hasMoreItems: false,
                      maxItems: null,
                      skipCount: null,
                      totalItems: this.files.length,
                    };
                  });
              }
              this.currentNode = currentNode;
              if (seanceId) {
                this.canUpload =
                  this.currentNode.allowableOperations?.includes("create") ||
                  false;
                this.children = response.children;
                this.pagination = response.pagination;

                if (this.children.length > 0) {
                  this.folders = this.children.filter(
                    (child) => child.isFolder,
                  );
                  this.files = this.children.filter((child) => child.isFile);
                } else {
                  this.folders = [];
                  this.files = [];
                  this.emptyFolder = true;
                }
                this.loading = false;
              }

              return currentNode;
            });
        });
    },
  },
});
