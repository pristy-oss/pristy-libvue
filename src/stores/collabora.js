/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";
import { useConfigStore } from "./config";
import axios from "axios";
import { newPristyError } from "../ErrorService";

export const useCollaboraStore = defineStore({
  id: "CollaboraStore",
  state: () => ({
    XMLExtensions: null,
    loaded: false,
  }),
  actions: {
    init() {
      console.log("Loading collabora extensions");
      return this.loadExtensionsXML()
        .then((extensions) => {
          this.XMLExtensions = [...extensions.getElementsByTagName("app")];
          this.loaded = true;
        })
        .catch((err) => {
          console.error("Fail loading collabora extensions", err);
        });
    },

    /**
     * Load XML file that contains extensions of collabora.
     */
    loadExtensionsXML() {
      const config = useConfigStore();
      console.log(
        `Loading collabora extensions: ${config.COLLABORA_HOSTING_DISCOVERY}`,
      );
      return axios
        .get(`${config.COLLABORA_HOSTING_DISCOVERY}`)
        .then((response) => {
          let parser = new DOMParser();
          return parser.parseFromString(response.data, "text/xml");
        })
        .catch((err) => {
          throw newPristyError("getCollaboraExtensions", err);
        });
    },

    checkIfExists(searchedMimetype, searchedAction) {
      if (!this.loaded) {
        return false;
      }

      const foundExtension = this.XMLExtensions.find(
        (extension) => extension.attributes["name"].value === searchedMimetype,
      );
      if (foundExtension) {
        const XMLActions = [...foundExtension.getElementsByTagName("action")];
        const foundAction = XMLActions.find(
          (action) => action.attributes["name"].value === searchedAction,
        );
        return !!foundAction;
      } else {
        return !!foundExtension;
      }
    },
  },
});
