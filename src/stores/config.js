/**
 *  Copyright (C) 2023 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";
import axios from "axios";
import { alfrescoApi } from "../AlfrescoApi";

export const useConfigStore = defineStore({
  id: "ConfigStore",
  state: () => ({
    APP_ROOT: "/",
    APP_ROOT_TITLE: "Pristy Root",
    ALFRESCO_HOST: "http://alfresco",
    ESPACES_HOST: "http://espaces",
    ACTES_HOST: "http://actes",
    PORTAIL_HOST: "http://portail",
    MARCHES_HOST: "http://marches",
    SOCIAL_HOST: "http://social",
    COLLABORA_HOSTING_DISCOVERY: "http://collabora:9980/hosting/discovery",
    ALFRESCO_AUTH: "BASIC",
    ALFRESCO_ROOT: "alfresco",
    AUTH: "basic",
    PREVIEW_URL: "http://localhost:80/portail/preview/",
    BREADCRUMB_ROOT_URL: "/mes-espaces",
    EDIT_WITH_MSOFFICE: false,
    NEW_PASSWORD_RULES: "no rules",
    MENU_ESPACES: false,
    MENU_ACTES: false,
    MENU_MARCHES: false,
    MENU_SOCIAL: false,
    WEBDELIB: false,
    DELIB_PUBLICATION: "add-tampon-actes",
    HIDE_SEANCE: false,
    CONSTRAINTS: [],
    METADATA: [],
    ASPECTS: [],
    PLAN_CLASSEMENT_AM: [],
    PLAN_CLASSEMENT_AF: [],
    PLAN_CLASSEMENT_ASE: [],
    NATURE_DOCUMENT_AF: [],
    NATURE_DOCUMENT_AM: [],
    NATURE_DOCUMENT_ASE: [],
    OIDC_REALM: "realm",
    OIDC_CLIENT_ID: "client_id",
    OIDC_PROVIDER: "https://auth.jeci.xyz/",
    OIDC_CUSTOM: {},
    OIDC_RESPONSE_TYPE: "id_token token",
    primeConfig: { ripple: true, inputStyle: "outlined" },
    ACA_LINK: "http://localhost:80/aca/",
    SHOW_ACA_LOGIN_LINK: false,
    SENTRY_ENABLE: false,
    SENTRY_DSN: "not-defined",
    MATOMO_ENABLE: false,
    MATOMO_INSTANCE_URL: "not-defined",
    MATOMO_SITE_ID: "not-defined",
    DEFAULT_ELEMENT_PER_PAGE: 5,
    ROW_PER_PAGE_OPTIONS: [],
    SEARCH_BASE_FILTER_QUERIES: [
      { query: "+TYPE:'cm:folder' OR +TYPE:'cm:content'" },
      {
        query:
          "-TYPE:'cm:thumbnail' AND -TYPE:'cm:failedThumbnail' AND -TYPE:'cm:rating'",
      },
      { query: "-cm:creator:System AND -QNAME:comment" },
      {
        query:
          "-TYPE:'st:site' AND -ASPECT:'st:siteContainer' AND -ASPECT:'sys:hidden'",
      },
      {
        query:
          "-TYPE:'dl:dataList' AND -TYPE:'dl:todoList' AND -TYPE:'dl:issue'",
      },
      { query: "-TYPE:'fm:topic' AND -TYPE:'fm:post' AND -TYPE:'fm:forum'" },
      { query: "-TYPE:'lnk:link'" },
      { query: "-PNAME:'0/wiki'" },
    ],
    SEARCH_FACET_QUERIES: [
      {
        label: "facets.queries.today",
        query: "cm:modified:[TODAY to TODAY]",
        group: "facets.categories.modified_date",
      },
      {
        label: "facets.queries.this_week",
        query: "cm:modified:[NOW/DAY-7DAYS TO NOW/DAY+1DAY]",
        group: "facets.categories.modified_date",
      },
      {
        label: "facets.queries.this_month",
        query: "cm:modified:[NOW/DAY-1MONTH TO NOW/DAY+1DAY]",
        group: "facets.categories.modified_date",
      },
      {
        label: "facets.queries.last_6_months",
        query: "cm:modified:[NOW/DAY-6MONTHS TO NOW/DAY+1DAY]",
        group: "facets.categories.modified_date",
      },
      {
        label: "facets.queries.this_year",
        query: "cm:modified:[NOW/DAY-1YEAR TO NOW/DAY+1DAY]",
        group: "facets.categories.modified_date",
      },
    ],
    SEARCH_FACET: [
      {
        field: "content.mimetype",
        mincount: 1,
        label: "facets.fields.file_type",
      },
      {
        field: "creator",
        mincount: 1,
        label: "facets.fields.creator",
      },
      {
        field: "modifier",
        mincount: 1,
        label: "facets.fields.modifier",
      },
      {
        field: "SITE",
        mincount: 1,
        label: "facets.fields.location",
      },
      {
        field: "TYPE",
        mincount: 1,
        label: "facets.fields.type",
      },
    ],
    SEARCH_USER_QUERY:
      '((cm:name:"%s*" OR cm:title:"%s*" OR cm:description:"%s*" OR TEXT:"%s*" OR TAG:"%s*"))',
  }),
  getters: {
    oidc: (state) => {
      if (state.AUTH === "keycloak" || state.AUTH === "lemonldap") {
        return {
          init: {
            // Use 'login-required' to always require authentication
            // If using 'login-required', there is no need for the router guards in router.js
            onLoad: "login-required",
            checkLoginIframe: false,

            // no redirectUrito use location.href
            redirectUri: undefined,
          },
        };
      }
      // No Oauth Configuartion
      return {};
    },
  },
  actions: {
    init(BASE_URL) {
      console.log(`Load config from ${BASE_URL}env-config.json`);
      return axios
        .get(`${BASE_URL}env-config.json`)
        .then((jsonConfig) => {
          this.$patch(jsonConfig.data);
        })
        .then(() => {
          console.log("String hostEcm to " + this.ALFRESCO_HOST);
          alfrescoApi.setConfig({
            hostEcm: this.ALFRESCO_HOST,
            authType: this.ALFRESCO_AUTH,
            contextRoot: this.ALFRESCO_ROOT,
          });
        });
    },
  },
});
