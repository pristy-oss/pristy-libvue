/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import axios from "axios";
import { defineStore, acceptHMRUpdate } from "pinia";
import { alfrescoApi, peopleApi, searchApi } from "../AlfrescoApi";
import { loginError } from "../ErrorService";
import { useConfigStore } from "./config";
import { webscriptApi } from "../AlfrescoApi";

// TODO: move to ConfigStore
const ALF_TOKEN_TTL_MS = 60 * 1000;

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export const useUserStore = defineStore({
  id: "UserStore",
  state: () => ({
    token: "",
    isAdmin: false,
    isLoggedIn: false,
    person: null,
    keycloak: null,
    homeFolder: null,
  }),
  getters: {
    displayName(state) {
      return state.person == null ? "" : state.person.displayName;
    },
  },
  actions: {
    /**
     * Logout current user and stop auto-renew for oidc connection
     * @return {Promise<axios.AxiosResponse<any>>}
     */
    logout() {
      console.log(`logout with ${this.email}`);

      if (alfrescoApi) {
        alfrescoApi.logout();
      }

      this.token = "";
      this.isAdmin = false;
      this.person = null;
      this.apiAlfresco = null;

      const config = useConfigStore();
      if (
        config.AUTH === "keycloak" ||
        config.AUTH === "lemonldap" ||
        config.AUTH === "azuread"
      ) {
        clearTimeout(this.keycloakRenewTO);

        return axios.get(`${config.ALFRESCO_HOST}/alfresco/logout`).then(() => {
          localStorage.removeItem("ticket-ECM");
          localStorage.removeItem("ACS_USERNAME");
          this.keycloak.logout();
        });
      }
    },
    /**
     * Check if the client is logged and get alf_ticket using Keycloak Bearer Token
     * @return {Promise<{id:string}>} Current alf_token
     */
    validateSession() {
      if (!alfrescoApi.isLoggedIn()) {
        this.isLoggedIn = false;
      }

      return this.getAlfTicket();
    },
    /**
     * Redirect to Keycloak if unauthent, else retrieve current Person
     * form Alfresco.
     * @return {Promise<void>}
     */
    connectKeycloak: async function () {
      // We wait for Keycloak init, then we can call all methods safely
      while (this.keycloak == null || this.keycloak.createLoginUrl === null) {
        await sleep(100);
      }
      if (this.keycloak.authenticated) {
        while (!this.isLoggedIn) {
          await sleep(100);
          await this.validateSession();
        }
        if (this.person === null) {
          await this.getPerson();
        }
      } else {
        const config = useConfigStore();
        if (config.AUTH === "azuread") {
          // AAD does not support "code id_token token"
          this.keycloak.responseType = config.OIDC_RESPONSE_TYPE;
        }
        const loginUrl = this.keycloak.createLoginUrl();
        window.location.replace(loginUrl);
      }
    },

    /**
     * Store Keycloak object and get alf_ticket using Keycloak Bearer Token.
     * We will ask alf_ticket every ALF_TOKEN_TTL_MS (60s)
     * @param _keycloak Keycloak object yet authenticate
     * @return {Promise<{id: string}>}
     */
    keycloakReady: function (_keycloak) {
      this.keycloak = _keycloak;
      return this.getAlfTicket().then((alfToken) => {
        this.keycloakRenewTO = setInterval(this.getAlfTicket, ALF_TOKEN_TTL_MS);
        return alfToken;
      });
    },
    /**
     * Log to Alfresco with Bearer token and set alf_token
     * @return {Promise<{id:string}>} Current alf_token
     */
    getAlfTicket() {
      const config = useConfigStore();
      if (
        config.AUTH !== "keycloak" &&
        config.AUTH !== "lemonldap" &&
        config.AUTH !== "azuread"
      ) {
        console.warn(
          "getAlfTicket() won't work in standard AUTH configuration",
        );
        return undefined;
      }
      return axios
        .get(
          `${config.ALFRESCO_HOST}/alfresco/api/-default-/public/authentication/versions/1/tickets/-me-`,
          {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              Authorization: `Bearer ${this.keycloak.token}`,
            },
          },
        )
        .then((response) => response.data.entry)
        .then((alftoken) => {
          alfrescoApi.setTicket(alftoken.id, null);
          window.localStorage.setItem(
            "ACS_USERNAME",
            this.keycloak.idTokenParsed.preferred_username,
          );
          this.isLoggedIn = true;
          return alftoken;
        })
        .catch((error) => {
          this.isLoggedIn = false;
          throw loginError(error);
        });
    },
    /**
     * Get Person for user '-me-'. Help to check if we are always authenticate
     * @return {Promise<Person>} Person object of current user.
     */
    getPerson() {
      return peopleApi
        .getPerson("-me-")
        .then((data) => data.entry)
        .then((p) => {
          this.isLoggedIn = true;
          this.person = p;
          return p;
        })
        .then((p) => {
          return searchApi
            .search({
              query: {
                language: "afts",
                query: "TYPE:'cm:person' AND cm:userName:" + p.id,
              },
              include: ["properties"],
            })
            .then((results) => {
              if (results.list.pagination.count > 1) {
                console.warn("More than one Person for " + p);
              } else if (results.list.pagination.count < 0) {
                console.warn("More than one Person for " + p);
                return {};
              }
              return results.list.entries[0].entry;
            })
            .then((person) => {
              this.homeFolder = person.properties["cm:homeFolder"];
            });
        })
        .catch(() => {
          this.isLoggedIn = false;
          return undefined;
        });
    },
    /**
     * Attempt to login a user
     * @param {string} user
     * @param {string} password
     */
    async login(user, password) {
      if (!user || !password) {
        return false;
      }

      await this.apiLogin(user, password);
    },
    /**
     * Do a Basic Authent to Alfresco
     * @param {string} a Usernamme
     * @param {string} p Password
     */
    apiLogin(a, p) {
      return alfrescoApi
        .login(a, p)
        .then(() => {
          this.isLoggedIn = true;
          return this.getPerson();
        })
        .catch((error) => {
          this.isLoggedIn = false;
          throw loginError(error);
        });
    },
    /**
     * Log Off Public Account ("user_publication_*")
     * @return {Promise<void>}
     */
    async publiclogoff() {
      const userProfile = localStorage.getItem("ACS_USERNAME");
      if (userProfile && userProfile.startsWith("user_publication_")) {
        alfrescoApi.invalidateSession();
        localStorage.removeItem("ticket-ECM");
        localStorage.removeItem("ACS_USERNAME");
      }
    },
    /**
     * Log In with the Public Account from a Public workspace.
     * @param espace_id Workspace ID that have a Public Account
     * @return {Promise<Awaited<string>>|Promise<string>} alf_ticket
     */
    publiclogin(espace_id) {
      let ticketECM = alfrescoApi.getTicketEcm();

      if (typeof ticketECM !== "undefined" && ticketECM != null) {
        // TODO check ticket validity
        return Promise.resolve(ticketECM);
      }
      return webscriptApi
        .executeWebScript(
          "GET",
          `fr/jeci/pristy/authentication/ticket/publication/site/${espace_id}`,
        )
        .then((alfticket) => {
          alfrescoApi.setTicket(alfticket.ticket, null);
          window.localStorage.setItem("ACS_USERNAME", alfticket.login);
          return alfticket.ticket;
        })
        .catch((error) => {
          throw loginError(error);
        });
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
}
