/*
  Copyright (C) 2022 - Jeci SARL - https://jeci.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";

/**
 * Store to keep current Theme
 *
 */
export const useThemeStore = defineStore({
  id: "ThemeStore",
  state: () => ({
    theme: "",
    indexScale: "",
    dark: false,
  }),
  actions: {
    initScale() {
      const savedIndex = localStorage.getItem("scaleIndex");
      let currentIndex = savedIndex || 2;
      this.updateScale(currentIndex);
    },
    updateScale(index) {
      const fontSizeValues = [12, 13, 14, 15, 16];
      const html = document.documentElement;
      html.style.fontSize = `${fontSizeValues[index]}px`;
      localStorage.setItem("scaleIndex", index);
      this.$patch({
        indexScale: index,
      });
    },
    changeTheme(selectedTheme) {
      const head = document.head;
      const link = document.getElementById("theme-link");

      const newLink = document.createElement("link");
      newLink.id = "theme-link";
      newLink.rel = "stylesheet";
      newLink.href = `https://cdnjs.cloudflare.com/ajax/libs/primevue/${
        import.meta.env.VITE_PRIMEVUE_VERSION
      }/resources/themes/${selectedTheme}/theme.min.css`;
      newLink.async = true;

      const onThemeLoaded = () => {
        console.log(`Theme ${selectedTheme} loaded`);
      };

      const onThemeError = (error) => {
        console.log(`Can’t load theme ${selectedTheme}`, error);
      };

      newLink.addEventListener("load", onThemeLoaded);
      newLink.addEventListener("error", onThemeError);

      if (link) {
        head.removeChild(link);
      }

      head.appendChild(newLink);

      localStorage.setItem("theme", selectedTheme);
      this.$patch({
        theme: selectedTheme,
        dark: selectedTheme.indexOf("dark") > 0,
      });
    },

    initTheme() {
      const savedTheme = localStorage.getItem("theme");
      let currentTheme = savedTheme || "lara-light-green";
      this.changeTheme(currentTheme);
    },
  },
});
