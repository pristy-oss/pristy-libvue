/*
  Copyright (C) 2022 - Jeci SARL - https://jeci.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";

/**
 * Store to keep selected node. Use to copy, move, share, etc.. actions.
 * @type {StoreDefinition<string, {nodes: [], folders: [], files: []}, {}, {selectNodes(*): void, resetSelection(): void, addNode(*): void, selectSingleNode(*): void, selectFiles(*): void, selectFolders(*): void, removeNode(*): void}>}
 */
export const useSelectionStore = defineStore({
  id: "SelectionStore",
  state: () => ({
    nodes: [],
    files: [],
    folders: [],
  }),
  actions: {
    resetSelection() {
      this.selectNodes([]);
    },
    /**
     * Add node to the current selection
     * @param node Node to add
     */
    addNode(node) {
      if (node) {
        this.selectNodes([...this.nodes, ...node]);
      }
    },
    /**
     * Remove node from the current selection
     * @param node Node to remove
     */
    removeNode(node) {
      if (node) {
        this.selectNodes(this.nodes.filter((n) => n.id !== node.id));
      }
    },
    /**
     * Replace current selection with a single node.
     * @param node The seelcted node.
     */
    selectSingleNode(node) {
      if (node) {
        this.selectNodes([node]);
      }
    },
    /**
     * Replace current selection with nodes.
     * @param selNodes List of nodes or undefined to reset current list.
     */
    selectNodes(selNodes) {
      if (selNodes) {
        this.$patch({
          nodes: selNodes,
          files: selNodes.filter((node) => node.isFile),
          folders: selNodes.filter((node) => node.isFolder),
        });
      }
    },
  },
});
