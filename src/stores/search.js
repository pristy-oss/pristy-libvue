/*
  Copyright (C) 2022 - Jeci SARL - https://jeci.fr

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { defineStore } from "pinia";
import { searchApi } from "../AlfrescoApi";
import { useConfigStore } from "./config";

export const useSearchStore = defineStore({
  id: "SearchStore",
  state: () => ({
    language: "afts",
    userQuery: "",
    aftsQuery: "",
    fullQuery: "",
    paging: {
      maxItems: "15",
      skipCount: "0",
    },
    include: ["properties", "path"],
    sort: null,
    defaultSort: [{ type: "FIELD", field: "score", ascending: false }],
    spellcheck: null,
    filterQueries: [],
    facetQueries: [],
    facets: [],
    fields: [],
    context: null,
    entries: [],
    pagination: {},
  }),
  getters: {
    facetFields(state) {
      return { facetFields: state.facets };
    },
  },
  actions: {
    reset() {
      const config = useConfigStore();

      this.language = "afts";
      this.userQuery = "";
      this.fullQuery = "";
      this.filterQueries = [...config.SEARCH_BASE_FILTER_QUERIES];
      this.facetQueries = [...config.SEARCH_FACET_QUERIES];
      this.facets = [...config.SEARCH_FACET];

      this.fields = [];
      this.context = null;
      this.entries = [];
      this.pagination = {};
    },
    defineUserQuery(userQuery) {
      const config = useConfigStore();
      let iQuery = config.SEARCH_USER_QUERY.replaceAll("%s", userQuery);
      this.language = "afts";
      this.userQuery = userQuery;
      let iFields = this.fields.map((f) => `(${f.query})`);
      this.fullQuery = [iQuery, ...iFields].join(" AND ");
    },

    queryStore(searchInput, page) {
      if (!searchInput) {
        return Promise.resolve();
      }

      this.paging.skipCount = page ? page * 15 : 0;

      this.defineUserQuery(searchInput);
      let optsQuery = {
        query: {
          language: this.language,
          userQuery: this.userQuery,
          query: this.fullQuery,
        },
        include: this.include,
        paging: this.paging,
        filterQueries: this.filterQueries.map((f) => {
          return { query: f.query };
        }),
        facetQueries: this.facetQueries,
        facetFields: {
          facets: this.facets.map((f) => {
            return {
              field: f.field,
              mincount: f.mincount,
              label: f.label,
            };
          }),
        },
        sort: this.sort || this.defaultSort,
        facetFormat: "V2",
      };
      return searchApi.search(optsQuery).then((results) => {
        this.context = results.list.context;
        if (page > 0) {
          this.entries = [...this.entries, ...results.list.entries];
        } else {
          this.entries = results.list.entries;
        }

        this.pagination = results.list.pagination;
      });
    },
    addSortField(sortField) {
      this.sort = sortField;
    },
    removeSortField() {
      this.sort = null;
    },
    /**
     *
     * @param bucket {query: ".." , label: ".."}
     * @returns {boolean}
     */
    addFilterQuery(bucket) {
      if (!bucket.query) {
        return false;
      }
      if (!this.filterQueries.find((field) => field.query === bucket.query)) {
        this.filterQueries.push({
          query: bucket.query,
          label: bucket.label,
        });

        return true;
      }
      return false;
    },
    removeFilterQuery(bucket) {
      this.filterQueries = this.filterQueries.filter((field) => {
        if (bucket.query.startsWith("(SITE:")) {
          if (
            field.label &&
            (field.label.startsWith("Dossier :") ||
              field.label.startsWith("Folder :"))
          ) {
            return false;
          }
        }
        return field.query !== bucket.query;
      });
    },
    /**
     *
     * @param bucket {query: ".." , label: ".."}
     * @returns {boolean}
     */
    addField(bucket) {
      if (!this.fields.find((field) => field.query === bucket.query)) {
        this.fields.push({
          query: bucket.query,
          label: bucket.label,
        });

        return true;
      }
      return false;
    },
    removeField(bucket) {
      this.fields = this.fields.filter((field) => field.query !== bucket.query);
    },
  },
});
