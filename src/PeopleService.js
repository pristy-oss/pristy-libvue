import { groupsApi, peopleApi, searchApi } from "./AlfrescoApi";
import { newPristyError } from "./ErrorService";

class PeopleService {
  getUser(userId) {
    const opts = {
      include: ["properties", "aspectNames", "capabilities"],
    };
    return peopleApi
      .getPerson(userId, opts)
      .then((user) => {
        return user.entry;
      })
      .catch((err) => {
        throw newPristyError("getUser", err);
      });
  }

  getUserWithOpts(userId, opts) {
    return peopleApi
      .getPerson(userId, opts)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        throw newPristyError("getUser", err);
      });
  }

  listGroups(userId) {
    const opts = {};
    return groupsApi
      .listGroupMembershipsForPerson(userId, opts)
      .then((groups) => {
        return groups.list.entries;
      })
      .catch((error) => {
        throw newPristyError("listGroups", error);
      });
  }

  listUsers() {
    const opts = {
      include: ["properties", "aspectNames", "capabilities"],
    };
    return peopleApi
      .listPeople(opts)
      .then((response) => {
        return response.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getAllUsers", err);
      });
  }

  listUsersWithOpts(opts) {
    /* const opts = {
      include: ["properties", "aspectNames", "capabilities"],
    }; */
    return peopleApi
      .listPeople(opts)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        throw newPristyError("getAllUsers", err);
      });
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  searchUser(userInput, sort) {
    const upperInput = userInput.toUpperCase();
    const lowerInput = userInput.toLowerCase();
    const firstLetterUpperInput = this.capitalizeFirstLetter(userInput);

    const generateSearchQueryWithPatterns = (
      field,
      values,
      patternPlacement = "suffix",
    ) => {
      return values
        .map((value) => {
          const formattedValue =
            patternPlacement === "surround" ? `*${value}*` : `${value}*`;
          return `${field}:"${formattedValue}"`;
        })
        .join(" OR ");
    };

    const inputs = [upperInput, lowerInput, firstLetterUpperInput];

    // Search user query (value*)
    const searchFieldsUser = [
      "cm:userName",
      "cm:firstName",
      "cm:lastName",
      "cm:email",
    ];
    const userQueryParts = searchFieldsUser.map((field) =>
      generateSearchQueryWithPatterns(field, inputs, "suffix"),
    );
    const searchUserQuery = `(TYPE:"cm:person" AND (${userQueryParts.join(" OR ")}))`;

    const query = {
      query: {
        language: "afts",
        query: `${searchUserQuery}`,
      },
      include: ["aspectNames", "properties"],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
      sort: sort,
    };
    return searchApi.search(query);
  }
}

export default new PeopleService();
