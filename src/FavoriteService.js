/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { favoritesApi } from "./AlfrescoApi";
import { default as menuService } from "./MenuService";
import { useUserStore } from "./stores/user";
import { useMenuStore } from "./stores/menu";

import { newPristyError } from "./ErrorService";

class FavoriteService {
  getFavorites(filter) {
    const user = useUserStore();
    const opts = {
      skipCount: 0,
      maxItems: 100,
      include: ["properties", "path"],
    };
    if (filter) {
      opts.where = `(EXISTS(target/${filter}))`;
    }
    return favoritesApi
      .listFavorites(user.person.id, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getFavorites", err);
      });
  }

  changeSingleFavorite(item) {
    if (item.isFavorite) {
      return this.deleteFavoris(item);
    } else {
      return this.createFavorite(item);
    }
  }

  createFavorite(item) {
    const user = useUserStore();
    const menuStore = useMenuStore();

    let favoriteBodyCreate = { target: {} };
    let guid = { guid: item.id };
    if (item.nodeType === "st:site") {
      favoriteBodyCreate.target = { site: guid };
    } else if (item.isFile) {
      favoriteBodyCreate.target = { file: guid };
    } else {
      favoriteBodyCreate.target = { folder: guid };
    }

    return favoritesApi
      .createFavorite(user.person.id, favoriteBodyCreate)
      .then((favorite) => {
        item.isFavorite = favorite.entry.targetGuid === item.id;
        return item;
      })
      .then((item) => {
        if (item.isFavorite) {
          menuStore.addFavorite(
            menuService.nodeToMenuItem(item),
            "menuFavoris",
          );
        }

        return item;
      })
      .catch((err) => {
        throw newPristyError("createFavorite", err);
      });
  }

  deleteFavoris(item) {
    const user = useUserStore();
    const menuStore = useMenuStore();
    return favoritesApi
      .deleteFavorite(user.person.id, item.id)
      .then(() => {
        item.isFavorite = false;
        return item;
      })
      .then((item) => {
        if (!item.isFavorite) {
          menuStore.removeFavorite(
            menuService.nodeToMenuItem(item),
            "menuFavoris",
          );
        }

        return item;
      })
      .catch((err) => {
        console.error(err);
        throw newPristyError("getSharedLink", err);
      });
  }
}

export default new FavoriteService();
