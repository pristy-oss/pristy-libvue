import favoriteService from "./FavoriteService";
import i18n from "./i18n/index.js";
import workspaceService from "./WorkspaceService";
import { useCollaboraStore } from "./stores/collabora";
import { useUserStore } from "./stores/user.js";

const t = i18n.global.t;

class MenuService {
  async computeMenuEspaces() {
    let menu = [];
    let favorites = await favoriteService.getFavorites();
    // Les espaces de travail favoris
    let favoriteSpaces = this.favoritesSiteToMenuItems(favorites);

    // les dossiers et fichiers favoris
    let favoriteOthers = this.favoritesNodeToMenuItems(
      favorites.filter(
        (favorite) =>
          Object.prototype.hasOwnProperty.call(favorite["target"], "folder") ||
          Object.prototype.hasOwnProperty.call(favorite["target"], "file"),
      ),
    );
    menu.push({
      label: t("menu.espaces"),
      items: favoriteSpaces,
      key: "menuEspaces",
      to: { name: "mes-espaces" },
      class: "layout-menuitem-root-text",
    });
    menu.push({
      label: t("menu.userHome"),
      key: "userHome",
      to: {
        name: "navigation-user-home",
        params: {
          IdUser: useUserStore().person.id,
        },
      },
      class: "layout-menuitem-root-text",
    });
    menu.push({
      label: t("menu.favoris"),
      key: "menuFavoris",
      class: "layout-menuitem-root-text",
      items: favoriteOthers,
    });
    menu.push({
      label: t("menu.recherche"),
      key: "menuRecherche",
      class: "layout-menuitem-root-text",
      to: {
        name: "recherche",
      },
    });
    return menu;
  }

  async computeMenuActes() {
    let menu = [];
    let actesSpacesResult = await workspaceService.searchAllWorkspaces(
      "AND pm:typeEspace:'actes'",
    );
    // tous les espaces actes
    let actesSpaces = this.actesSiteToMenuItems(actesSpacesResult);
    menu.push({
      label: "Actes administratifs",
      key: "menuActes",
      to: { name: "mes-espaces-actes" },
      class: "layout-menuitem-root-text",
      items: actesSpaces,
    });
    return menu;
  }

  actesSiteToMenuItems(actesWorkspaces) {
    let actesItems = actesWorkspaces.map((acteWorkspace) => {
      return {
        label: acteWorkspace.title,
        class: "menu-item responsive-text",
        icon: "ri-folder-2-line",
        to: { name: "seances", params: { id: acteWorkspace.name } },
      };
    });
    actesItems.push({
      label: "Recherche",
      icon: "ri-search-line",
      class: "menu-item responsive-text",
      to: { name: "recherche-actes" },
    });
    return actesItems;
  }

  favoritesSiteToMenuItems(favorites) {
    return favorites
      .filter(
        (favorite) =>
          Object.prototype.hasOwnProperty.call(favorite["target"], "site") &&
          !favorite?.properties?.["pm:typeEspace"],
      )
      .map((favorite) => {
        return {
          label: favorite.target.site.title,
          icon: "ri-building-line",
          class: "menu-item responsive-text",
          to: {
            name: "navigation",
            params: { id: favorite.target.site.id },
          },
        };
      });
  }

  favoritesNodeToMenuItems(favorites) {
    return favorites.reduce((favoriteItems, favorite) => {
      if (!favorite.target) {
        console.warn(
          "favoritesNodeToMenuItems, favorite has no target",
          favorite,
        );
        return favoriteItems;
      }
      const target = favorite.target;
      const targetType = target.folder || target.file || target.site;

      if (!targetType || !targetType.path) {
        console.warn(
          "favoritesNodeToMenuItems, target has no valid path",
          favorite,
        );
        return favoriteItems;
      }

      const sitePath = targetType.path.elements.find(
        (element) => element.nodeType === "st:site",
      );

      if (sitePath?.name) {
        let favoriteItem = this.retrieveFavorite(favorite);
        favoriteItems.push(favoriteItem);
      }
      return favoriteItems;
    }, []);
  }

  /**
   * Transforms a SiteEntry in an object corresponding to VueJS Menu component items
   * @param favorite to add in menu
   * @returns a MenuItem
   */
  siteToMenuItem(favorite) {
    let isActe = favorite.properties?.["pm:typeEspace"] === "actes";
    return {
      label: favorite.title,
      icon: isActe ? "ri-folder-2-line" : "ri-building-line",
      class: "menu-item responsive-text",
      to: isActe
        ? {
            name: "seances",
            params: { id: favorite.name },
          }
        : {
            name: "navigation",
            params: { id: favorite.name },
          },
    };
  }

  retrieveFavorite(favorite) {
    const favoriteTarget = favorite.target;
    const favoriteTargetType =
      favoriteTarget.folder || favoriteTarget.file || favoriteTarget.site;
    if (!favoriteTargetType.path) {
      console.warn("nodeToMenuItem, favorite has no path", favorite);
      return;
    }

    let target = "";
    const sitePath = favoriteTargetType.path.elements.find(
      (element) => element.nodeType === "st:site",
    );
    let route;
    if (favoriteTargetType.isFolder) {
      route = {
        name: "navigation-dossier",
        params: { id: sitePath.name, FolderId: favorite.targetGuid },
      };
    } else {
      route = this.getTargetRoute(favoriteTargetType);
      if (route.name === "edit-collabora") {
        target = "_blank";
      }
    }
    if (sitePath?.name) {
      return {
        label: favoriteTargetType.name,
        icon: favoriteTargetType.isFolder ? "ri-folder-2-line" : "ri-file-line",
        class: "menu-item responsive-text",
        target: target,
        to: route,
      };
    }
  }

  /**
   * Transforms a NodeEntry in an object corresponding to VueJS Menu component items
   * @param favorite to add in menu
   * @returns a MenuItem
   */
  nodeToMenuItem(favorite) {
    if (!favorite.path) {
      console.warn("nodeToMenuItem, favorite has no path", favorite);
      return;
    }

    let target = "";
    const sitePath = favorite.path.elements.find(
      (element) => element.nodeType === "st:site",
    );
    let route;
    if (favorite.isFolder) {
      route = {
        name: "navigation-dossier",
        params: { id: sitePath.name, FolderId: favorite.id },
      };
    } else {
      route = this.getTargetRoute(favorite);
      if (route.name === "edit-collabora") {
        target = "_blank";
      }
    }
    if (sitePath?.name) {
      return {
        label: favorite.name,
        icon: favorite.isFolder ? "ri-folder-2-line" : "ri-file-line",
        class: "menu-item responsive-text",
        target: target,
        to: route,
      };
    }
  }

  getTargetRoute(item) {
    const collaboraStore = useCollaboraStore();

    if (item.content.mimeType.startsWith("image/")) {
      return { name: "consultation-image", params: { id: item.id } };
    } else if (item.content.mimeType.startsWith("video/")) {
      return { name: "consultation-video", params: { id: item.id } };
    } else if (collaboraStore.checkIfExists(item.content.mimeType, "edit")) {
      return {
        name: "edit-collabora",
        params: { action: "edit", id: item.id },
      };
    } else if (collaboraStore.checkIfExists(item.content.mimeType, "view")) {
      return {
        name: "edit-collabora",
        params: { action: "view", id: item.id },
      };
    } else if (
      collaboraStore.checkIfExists(item.content.mimeType, "view_comment")
    ) {
      return {
        name: "edit-collabora",
        params: { action: "view", id: item.id },
      };
    }
    return { name: "consultation-pdf", params: { id: item.id } };
  }
}

export default new MenuService();
