/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */
import i18n from "./i18n/index.js";

const t = i18n.global.t;

class PristyError extends Error {
  constructor(message, code, cause) {
    super(message);
    this.code = code;
    this.cause = cause;
  }
}

function translateErrorMessage(prefix, cause) {
  let message = t("error." + prefix + "." + cause.status);
  if (!message) {
    message = t("error." + cause.status);
  }
  if (!message) {
    message = cause.message;
  }
  return message;
}

export function newPristyError(prefix, error) {
  let message = translateErrorMessage(prefix, error);
  return new PristyError(message, error.status, error);
}

// TODO rename to TranslateErrorService
class ErrorService {
  updateNodeError(error) {
    if (error.status === 409) {
      let parsedMessage;
      try {
        parsedMessage =
          typeof error.message === "string"
            ? JSON.parse(error.message)
            : error.message;
      } catch (e) {
        console.error("Erreur de parsing JSON :", e);
        parsedMessage = {};
      }

      // Si parsedMessage est un objet, vérifie si briefSummary contient les mots
      const messageToCheck = parsedMessage?.error?.briefSummary || "";

      if (
        messageToCheck.includes("verrouillé") ||
        messageToCheck.includes("locked")
      ) {
        throw newPristyError(t("updateNodeLocked"), error);
      }
    }
    throw newPristyError("updateNode", error);
  }

  uploadFileError(error, filename) {
    if (error.status === 409) {
      throw new PristyError(
        t("error.uploadFile.409", { filename: filename }),
        error.status,
        error,
      );
    }

    throw newPristyError("uploadFile", error);
  }

  deleteNodeError(error, type) {
    switch (error.status) {
      case 403:
      case 404:
      case 409:
        throw new PristyError(
          t(`error.deleteNode.${error.status}`, { type: t(type) }),
          error.status,
          error,
        );

      default:
        throw newPristyError("deleteNode", error);
    }
  }

  getUploadErrors(listError) {
    let errors = [];
    listError.forEach((error) => {
      errors.push({
        severity: "error",
        content: t("uploadUnknownError", {
          error: error.message,
        }),
      });
    });
    return errors.filter((error, index) => {
      return (
        index ===
        errors.findIndex((otherError) => {
          return error.content === otherError.content;
        })
      );
    });
  }
}

export function loginError(data) {
  let caughtError = data.error ? data.error.response : JSON.parse(data.message);
  if (caughtError.statusCode === undefined) {
    caughtError = caughtError.error;
  }
  switch (caughtError.statusCode) {
    case 400:
      caughtError.message = t("error.login.400");
      break;
    case 401:
      caughtError.message = t("error.login.401");
      break;
    case 403:
      caughtError.message = t("error.login.403");
      break;
    case 502:
      caughtError.message = t("alfrescoError");
      break;
    default:
      caughtError.message = caughtError.statusText
        ? caughtError.statusText
        : caughtError.message;
  }
  console.warn("loginError", caughtError);
  return caughtError;
}

export default new ErrorService();
