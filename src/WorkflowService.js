/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import axios from "axios";
import i18n from "./i18n/index.js";

import { formatDate } from "@vueuse/core";

const t = i18n.global.t;
class WorkflowService {
  //Process Methods
  createProcess(opts) {
    const url = `/alfresco/api/-default-/public/workflow/versions/1/processes`;
    return axios
      .post(url, opts, {
        headers: {
          Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
          Accept: "application/json",
        },
      })
      .catch((error) => {
        console.error(t("tasks.error.createProcess"), error);
        throw new Error(t("tasks.error.createProcess"));
      });
  }

  getProcesses() {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/processes?where=(startUserId='${localStorage.getItem("ACS_USERNAME")}')`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.list.entries;
      })
      .catch((error) => {
        console.error(t("tasks.error.getProcesses"), error);
        throw new Error(t("tasks.error.getProcesses"));
      });
  }

  getProcess(processId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/processes/${processId}`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.entry;
      })
      .catch((error) => {
        console.error(t("tasks.error.getProcess"), error);
        throw new Error(t("tasks.error.getProcess"));
      });
  }

  getFinishedProcesses() {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/processes?where=(status=completed and startUserId='${localStorage.getItem("ACS_USERNAME")}')`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.list.entries;
      })
      .catch((error) => {
        console.error(t("tasks.error.getFinishedProcess"), error);
        throw new Error(t("tasks.error.getFinishedProcess"));
      });
  }

  addProcessItems(processId, opts) {
    const url = `/alfresco/api/-default-/public/workflow/versions/1/processes/${processId}/items`;
    return axios
      .post(url, opts, {
        headers: {
          Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
          Accept: "application/json",
        },
      })
      .catch((error) => {
        console.error(t("tasks.error.addProcessItems"), error);
        throw new Error(t("tasks.error.addProcessItems"));
      });
  }

  getProcessItems(processId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/processes/${processId}/items`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.list.entries;
      })
      .catch((error) => {
        console.error(t("tasks.error.getProcessItems"), error);
        throw new Error(t("tasks.error.getProcessItems"));
      });
  }

  getProcessVariables(processId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/processes/${processId}/variables`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.getProcessVariables"), error);
        throw new Error(t("tasks.error.getProcessVariables"));
      });
  }

  getProcessDefinition(processDefinitionId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/process-definitions/${processDefinitionId}`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.entry;
      })
      .catch((error) => {
        console.error(t("tasks.error.getProcessDefinition"), error);
        throw new Error(t("tasks.error.getProcessDefinition"));
      });
  }
  cancelWorkflow(processId) {
    return axios
      .delete(
        `/alfresco/api/-default-/public/workflow/versions/1/processes/${processId}`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.cancelWorkflow"), error);
        throw new Error(t("tasks.error.cancelWorkflow"));
      });
  }
  openProcessDiagram(processDefinitionId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/process-definitions/${processDefinitionId}/image`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
          responseType: "blob",
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.openProcessDiagram"), error);
        throw new Error(t("tasks.error.openProcessDiagram"));
      });
  }

  getProcessHistoryTasks(processId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks?where=(processId=${processId} and status=any)`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.getProcessHistoryTasks"), error);
        throw new Error(t("tasks.error.getProcessHistoryTasks"));
      });
  }

  // Tasks methods
  listTasks() {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks?where=(assignee='${localStorage.getItem("ACS_USERNAME")}')`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        const tasksList = [];
        response.data.list.entries.forEach((task) => {
          tasksList.push(task.entry);
        });
        return tasksList;
      })
      .catch((error) => {
        console.error(t("tasks.error.listTasks"), error);
        throw new Error(t("tasks.error.listTasks"));
      });
  }
  listFinishedTasks() {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks?where=(status=completed and assignee='${localStorage.getItem("ACS_USERNAME")}')`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        const tasksList = [];
        response.data.list.entries.forEach((task) => {
          tasksList.push(task.entry);
        });
        return tasksList;
      })
      .catch((error) => {
        console.error(t("tasks.error.listFinishedTasks"), error);
        throw new Error(t("tasks.error.listFinishedTasks"));
      });
  }

  getTask(taskId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .then((response) => {
        return response.data.entry;
      })
      .catch((error) => {
        console.error(t("tasks.error.getTask"), error);
        throw new Error(t("tasks.error.getTask"));
      });
  }

  endTask(taskId) {
    const variables = {
      state: "completed",
    };
    const url = `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}?select=state`;
    return axios
      .put(url, variables, {
        headers: {
          Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
      .catch((error) => {
        console.error(t("tasks.error.endTask"), error);
        throw new Error(t("tasks.error.endTask"));
      });
  }

  getTaskVariables(taskId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}/variables`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.getTaskVariables"), error);
        throw new Error(t("tasks.error.getTaskVariables"));
      });
  }

  postTaskVariables(taskId, variables) {
    const url = `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}/variables`;
    return axios
      .post(url, variables, {
        headers: {
          Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
          Accept: "application/json",
        },
      })
      .catch((error) => {
        console.error(t("tasks.error.postTaskVariables"), error);
        throw new Error(t("tasks.error.postTaskVariables"));
      });
  }

  updateTaskVariables(taskId, variables) {
    const url = `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}?select=state,variables`;
    return axios
      .put(url, variables, {
        headers: {
          Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
      .catch((error) => {
        console.error(t("tasks.error.updateTaskVariables"), error);
        throw new Error(t("tasks.error.updateTaskVariables"));
      });
  }

  getTaskItem(taskId) {
    return axios
      .get(
        `/alfresco/api/-default-/public/workflow/versions/1/tasks/${taskId}/items`,
        {
          headers: {
            Authorization: `Basic ${btoa(`ROLE_TICKET:${localStorage.getItem("ticket-ECM")}`)}`,
            Accept: "application/json",
          },
        },
      )
      .catch((error) => {
        console.error(t("tasks.error.getTaskItem"), error);
        throw new Error(t("tasks.error.getTaskItem"));
      });
  }

  //Utils methods
  getSeverityFromPriotity(priority) {
    switch (priority) {
      case 1:
        return "danger";
      case 2:
        return "success";
      case 3:
        return "info";
      default:
        return "warning";
    }
  }

  formatDate(value) {
    return value
      ? formatDate(new Date(value), "ddd D MMM YYYY")
      : t("tasks.none");
  }

  formatDateWithHour(dateString) {
    const date = new Date(dateString);

    // Obtenez les parties de la date
    const day = String(date.getUTCDate()).padStart(2, "0");
    const monthIndex = date.getUTCMonth();
    const year = date.getUTCFullYear();
    const hours = String(date.getUTCHours()).padStart(2, "0");
    const minutes = String(date.getUTCMinutes()).padStart(2, "0");
    const seconds = String(date.getUTCSeconds()).padStart(2, "0");
    const daysOfWeek = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
    const dayName = daysOfWeek[date.getUTCDay()];
    const months = [
      "Jan",
      "Fev",
      "Mar",
      "Avr",
      "Mai",
      "Jui",
      "Jui",
      "Aoû",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    const monthName = months[monthIndex];
    return `${dayName} ${day} ${monthName} ${year} ${hours}:${minutes}:${seconds}`;
  }

  displayPriority(priority) {
    const priorityMap = {
      3: t("tasks.priority.low"),
      2: t("tasks.priority.medium"),
      1: t("tasks.priority.high"),
    };
    return priorityMap[priority] || t("tasks.priority.notFound");
  }

  getTaskType(task) {
    if (!task || !task.activityDefinitionId) {
      return null;
    }
    switch (task.activityDefinitionId) {
      case "approved":
        return t("tasks.approved");
      case "rejected":
        return t("tasks.rejected");
      case "reviewTask":
        return t("tasks.reviewTask");
      case "adhocTask":
        return t("tasks.adhocTask");
      case "verifyTaskDone":
        return t("tasks.verifyTaskDone");
      default:
        return t("tasks.unknownTask");
    }
  }

  getTaskStatusLabelFromCode(code) {
    const statusOptions = [
      { label: t("tasks.statusLabel.notYetStarted"), code: "Not Yet Started" },
      { label: t("tasks.statusLabel.inProgress"), code: "In Progress" },
      { label: t("tasks.statusLabel.onHold"), code: "On Hold" },
      { label: t("tasks.statusLabel.cancelled"), code: "Cancelled" },
      { label: t("tasks.statusLabel.completed"), code: "Completed" },
    ];
    const status = statusOptions.find((option) => option.code === code);
    return status ? status.label : "Unknown";
  }
}
export default new WorkflowService();
