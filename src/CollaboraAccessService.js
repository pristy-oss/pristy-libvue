/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */
import axios from "axios";
import { newPristyError } from "./ErrorService";

class CollaboraAccessService {
  getLoolUrl(host) {
    let path = `${host}/alfresco/service/lool/host/url`;
    return axios
      .get(path)
      .then((resp) => {
        return resp.data;
      })
      .catch((error) => {
        throw newPristyError("getLoolUrl", error);
      });
  }
  getAccessToken(host, nodeId, action) {
    let path = `${host}/alfresco/service/lool/token?nodeRef=workspace://SpacesStore/${nodeId}&action=${action}`;
    let headers = {
      headers: {
        Authorization: `basic ${window.btoa(
          window.localStorage["ticket-ECM"],
        )}`,
      },
    };
    return axios
      .get(path, headers)
      .then((resp) => {
        return resp.data;
      })
      .catch((error) => {
        throw newPristyError("getAccessToken", error);
      });
  }
}

export default new CollaboraAccessService();
