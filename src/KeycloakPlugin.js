import Keycloak from "keycloak-js";

const vueKeyCloak = {
  install(app, options) {
    console.log("load Keycloak With", options.init);
    const keycloak = new Keycloak(options.init);

    keycloak.onReady = () => {
      if (typeof options.onReady === "function") {
        options.onReady(keycloak);
      }
    };

    let updateTokenInterval;

    keycloak.onAuthSuccess = () => {
      updateTokenInterval = setInterval(() => {
        keycloak
          .updateToken(60)
          .then((updated) => {
            if (options.init.enableLogging) {
              console.log(
                `[vue-keycloak-js] Token ${updated ? "updated" : "not updated"}`,
              );
            }
          })
          .catch((error) => {
            if (options.init.enableLogging) {
              console.error("[vue-keycloak-js] Error updating token:", error);
            }
            keycloak.clearToken();
          });
      }, 10000);
    };

    keycloak.onAuthLogout = () => {
      clearInterval(updateTokenInterval);
      if (typeof options.onAuthLogout === "function") {
        options.onAuthLogout(keycloak);
      }
    };

    keycloak
      .init(options.init)
      .then((authenticated) => {
        if (typeof options.onInitSuccess === "function") {
          options.onInitSuccess(authenticated);
        }
      })
      .catch((err) => {
        const error = new Error("Failure during initialization of Keycloak");
        if (typeof options.onInitError === "function") {
          options.onInitError(error, err);
        } else {
          console.error(error, err);
        }
      });
  },
};

export default vueKeyCloak;
