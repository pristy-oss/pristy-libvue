/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */
import i18n from "./i18n/index.js";

const t = i18n.global.t;

class IconeService {
  getIcon(item) {
    let icon;
    if (item.isFolder) {
      icon = "mimetype_folder";
    } else if (item.isFile) {
      if (item.content.mimeType.startsWith("image/")) {
        icon = "mimetype_image";
      } else if (item.content.mimeType.startsWith("video/")) {
        icon = "mimetype_video";
      } else {
        switch (item.content.mimeType) {
          case "application/pdf":
            icon = "mimetype_pdf";
            break;
          case "message/rfc822":
            icon = "mimetype_eml";
            break;
          case "application/vnd.oasis.opendocument.text":
          case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            icon = "collabora-text";
            break;
          case "application/vnd.ms-excel":
          case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
          case "application/vnd.oasis.opendocument.spreadsheet":
            icon = "collabora-sheet";
            break;
          case "application/vnd.oasis.opendocument.graphics":
            icon = "collabora-draw";
            break;
          case "application/vnd.oasis.opendocument.presentation":
          case "application/vnd.ms-powerpoint":
          case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
            icon = "collabora-presentation";
            break;
          default:
            icon = "default";
        }
      }
    } else {
      switch (item.nodeType) {
        case "st:site":
          icon = "mimetype_espace";
          break;
        default:
          icon = "default";
      }
    }

    return icon;
  }

  getIconLabel(item) {
    let label;
    if (item.isFolder) {
      label = t("dossier");
    } else if (item.isFile) {
      if (item.content.mimeType.startsWith("image/")) {
        label = t("fichier_image");
      } else if (item.content.mimeType.startsWith("video/")) {
        label = t("fichier_video");
      } else {
        switch (item.content.mimeType) {
          case "application/pdf":
            label = t("fichier_pdf");
            break;
          case "message/rfc822":
            label = t("fichier_eml");
            break;
          case "application/vnd.oasis.opendocument.text":
          case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
            label = t("fichier_texte");
            break;
          case "application/vnd.ms-excel":
          case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
          case "application/vnd.oasis.opendocument.spreadsheet":
            label = t("fichier_tableur");
            break;
          case "application/vnd.oasis.opendocument.graphics":
            label = t("fichier_dessin");
            break;
          case "application/vnd.oasis.opendocument.presentation":
          case "application/vnd.ms-powerpoint":
          case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
            label = t("fichier_presentation");
            break;
          default:
            label = item.nodeType;
        }
      }
    } else {
      switch (item.nodeType) {
        case "st:site":
          label = t("espace");
          break;
        default:
          label = item.nodeType;
      }
    }
    return label;
  }
}

export default new IconeService();
