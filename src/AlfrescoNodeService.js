import {
  nodesApi,
  searchApi,
  sharedLinksApi,
  versionsApi,
} from "./AlfrescoApi";
import errorService, { newPristyError } from "./ErrorService";
import ErrorService from "./ErrorService";

class AlfrescoNodeService {
  getNode(id) {
    const opts = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
        "path",
      ],
    };

    return nodesApi.getNode(id, opts).then((data) => {
      return data.entry;
    });
  }

  getNodeContent(nodeId, attachment) {
    const opts = {
      attachment: attachment,
    };
    return nodesApi.getNodeContent(nodeId, opts).catch((error) => {
      throw newPristyError("getVersionContent", error);
    });
  }

  createNode(id, opts) {
    return nodesApi.createNode(id, opts).catch((error) => {
      throw newPristyError("createNode", error);
    });
  }

  getChildren(id) {
    const opts = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
        "path",
        "isLocked",
      ],
      maxItems: localStorage.getItem("elementPerPage"),
    };

    return nodesApi.listNodeChildren(id, opts).then((data) => {
      let children = data.list.entries.map((data) => data.entry);
      let documentLibrary = children.filter(
        (child) => child.name === "documentLibrary",
      )[0];
      if (documentLibrary) {
        return nodesApi
          .listNodeChildren(documentLibrary.id, opts)
          .then((subData) => {
            return {
              documentLibrary: documentLibrary,
              children: subData.list.entries.map((data) => data.entry),
              pagination: subData.list.pagination,
            };
          });
      } else {
        return {
          children: children,
          pagination: data.list.pagination,
        };
      }
    });
  }

  getChildrenWithOpts(id, opts) {
    const include = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
        "path",
        "isLocked",
      ],
    };
    return nodesApi.listNodeChildren(id, include).then((data) => {
      let children = data.list.entries.map((data) => data.entry);
      let documentLibrary = children.filter(
        (child) => child.name === "documentLibrary",
      )[0];
      if (documentLibrary) {
        return nodesApi
          .listNodeChildren(documentLibrary.id, opts)
          .then((subData) => {
            return {
              documentLibrary: documentLibrary,
              children: subData.list.entries.map((data) => data.entry),
              pagination: subData.list.pagination,
            };
          });
      } else {
        return nodesApi.listNodeChildren(id, opts).then((data) => {
          return {
            children: data.list.entries.map((data) => data.entry),
            pagination: data.list.pagination,
          };
        });
      }
    });
  }

  getDirectChildren(id) {
    const opts = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
        "path",
        "isLocked",
      ],
      maxItems: 1000,
    };

    return nodesApi.listNodeChildren(id, opts).then((data) => {
      let children = data.list.entries.map((data) => data.entry);
      return {
        children: children,
        pagination: data.list.pagination,
      };
    });
  }

  updateNode(id, opts) {
    return nodesApi.updateNode(id, opts).catch((error) => {
      throw ErrorService.updateNodeError(error);
    });
  }

  /**
   * Create a version if not existe.
   * @param id
   */
  checkVersion(id) {
    return nodesApi
      .getNode(id)
      .then((data) => {
        return data.entry;
      })
      .then((node) => {
        if (!node.aspectNames.find((aspect) => aspect === "cm:versionable")) {
          // Add aspect
          const opts = { aspectNames: [...node.aspectNames, "cm:versionable"] };
          return nodesApi
            .updateNode(id, opts)
            .then(() => {
              // Iniale Version
              return "1.0";
            })
            .catch((error) => {
              if (error.status === 403) {
                console.warn("Can’t add version - Forbidden");
              } else {
                throw newPristyError("updateNode", error);
              }
            });
        }
      });
  }

  deleteNode(id, type) {
    const opts = {};

    return nodesApi.deleteNode(id, opts).catch((error) => {
      errorService.deleteNodeError(error, type);
    });
  }

  getParents(id) {
    const opts = {
      include: ["isFavorite", "properties", "path"],
    };

    return nodesApi.listParents(id, opts).then((data) => {
      let parents = data.list.entries.map((data) => data.entry);
      let documentLibrary = parents[0];
      if (documentLibrary && documentLibrary.name === "documentLibrary") {
        return this.getParents(documentLibrary.id).then((supParent) => {
          return {
            documentLibrary: documentLibrary,
            supParent: supParent,
          };
        });
      } else {
        return parents[0];
      }
    });
  }

  getDocumentLibrary(id) {
    const opts = {
      include: [
        "isFavorite",
        "properties",
        "allowableOperations",
        "permissions",
      ],
    };

    return nodesApi.listNodeChildren(id, opts).then((data) => {
      let children = data.list.entries.map((data) => data.entry);
      return children.find((child) => child.name === "documentLibrary");
    });
  }

  updateContentNode(id, bin, version, comment) {
    let opts = {
      majorVersion: version,
      comment: comment,
    };
    return nodesApi.updateNodeContent(id, bin, opts).catch((error) => {
      throw newPristyError("updateContentNode", error);
    });
  }

  copyNode(id, destination, name) {
    const nodeBody =
      name !== null
        ? {
            targetParentId: destination,
            name: name,
          }
        : {
            targetParentId: destination,
          };

    const opts = {
      include: ["path"],
    };

    return nodesApi.copyNode(id, nodeBody, opts).catch((error) => {
      throw newPristyError("copyNode", error);
    });
  }

  moveNode(id, destination, name) {
    const nodeBody =
      name !== null
        ? {
            targetParentId: destination,
            name: name,
          }
        : {
            targetParentId: destination,
          };

    const opts = {
      include: ["path"],
    };

    return nodesApi.moveNode(id, nodeBody, opts).catch((error) => {
      throw newPristyError("moveNode", error);
    });
  }

  createSharedLink(node) {
    const sharedLinkBody = {
      nodeId: node.id,
      expiresAt: null,
    };
    return sharedLinksApi.createSharedLink(sharedLinkBody).catch((error) => {
      throw newPristyError("createSharedLink", error);
    });
  }

  deleteSharedLink(linkId) {
    return sharedLinksApi.deleteSharedLink(linkId).catch((error) => {
      throw newPristyError("deleteSharedLink", error);
    });
  }

  revertVersion(nodeId, versionId, comment) {
    const revertBody = {
      majorVersion: false,
      comment: comment,
    };
    const opts = {
      include: ["properties"],
    };

    return versionsApi
      .revertVersion(nodeId, versionId, revertBody, opts)
      .catch((error) => {
        throw newPristyError("revertVersion", error);
      });
  }

  listNodeVersionHistory(nodeId) {
    const opts = {
      include: ["properties"],
    };

    return versionsApi.listVersionHistory(nodeId, opts).catch((error) => {
      throw newPristyError("listNodeVersionHitory", error);
    });
  }

  getVersionContent(nodeId, versionId, attachment) {
    const opts = {
      attachment: attachment,
    };
    return versionsApi
      .getVersionContent(nodeId, versionId, opts)
      .catch((error) => {
        throw newPristyError("getVersionContent", error);
      });
  }

  getSharedLink(shareId) {
    return sharedLinksApi.getSharedLink(shareId).catch((error) => {
      throw newPristyError("sharedLink", error);
    });
  }

  getSharedLinkContent(shareId) {
    const opts = {
      attachment: true,
    };
    return sharedLinksApi.getSharedLinkContent(shareId, opts).catch((error) => {
      throw newPristyError("sharedLinkContent", error.error);
    });
  }

  getSharedLinkRenditionContent(shareId) {
    return sharedLinksApi
      .getSharedLinkRenditionContent(shareId, "pdf")
      .catch((error) => {
        throw newPristyError("sharedLinkRenditionContent", error.error);
      });
  }
  getHomeFolderFromUser(idUser) {
    const query = {
      query: {
        language: "afts",
        query: `TYPE:"cm:person" AND cm:userName:${idUser}`,
      },
      include: ["permissions", "properties", "allowableOperations", "path"],
    };
    return searchApi.search(query).then((response) => {
      return response;
    });
  }
  getUserHomeNode() {
    const query = {
      query: {
        language: "afts",
        query: `PATH:"//app:company_home/app:user_homes"`,
      },
      include: ["permissions", "properties", "allowableOperations", "path"],
    };
    return searchApi.search(query);
  }
  isFolderInUserHome(nodeId, homeFolderId) {
    return this.getNode(nodeId)
      .then((node) => {
        return node.path.elements.some(
          (element) => element.id === homeFolderId,
        );
      })
      .catch((error) => {
        console.error("Error fetching node:", error);
        return false;
      });
  }
}

export default new AlfrescoNodeService();
