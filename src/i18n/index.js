/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import { createI18n } from "vue-i18n";
import { messages } from "./locales";

let language =
  localStorage?.getItem("user.language") || window?.navigator?.language || "fr";

const i18n = createI18n({
  locale: language,
  // translations
  messages,
  fallbackLocale: "fr",
  silentTranslationWarn: true,
  silentFallbackWarn: true,
});
export default i18n;
