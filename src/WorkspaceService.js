/**
 *  Copyright (C) 2022 - Jeci SARL - https://jeci.fr
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see https://www.gnu.org/licenses/agpl-3.0.html.
 */

import {
  groupsApi,
  nodesApi,
  peopleApi,
  searchApi,
  sitesApi,
} from "./AlfrescoApi";
import { newPristyError } from "./ErrorService";

class WorkspaceService {
  getWorkspaces() {
    const opts = {
      skipCount: 0,
      maxItems: 1000,
    };

    return sitesApi
      .listSites(opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getWorkspaces", err);
      });
  }

  searchAllWorkspaces(extraQuery = "") {
    const query = {
      query: {
        //language: "afts",
        query: "TYPE: 'st:site' " + extraQuery,
      },
      include: ["allowableOperations", "properties", "aspectNames"],
      paging: {
        maxItems: 1000,
        skipCount: 0,
      },
      localization: { locales: ["fr_FR"] },
    };

    return searchApi
      .search(query)
      .then((data) => {
        return data.list.entries.map((data) => {
          let obj = data.entry;
          // add some basic properties to match sitesApi.listSites output
          obj.title = data.entry.properties["cm:title"];
          obj.visibility = data.entry.properties["st:siteVisibility"];
          obj.typeEspace = data.entry.properties["pm:typeEspace"];
          return obj;
        });
      })
      .catch((err) => {
        throw newPristyError("searchAllWorkspaces", err);
      });
  }

  /**
   * Call sitesApi.getSite to get Site
   * @param id siteid
   * @returns {Promise<Site>}
   */
  getWorkspace(id) {
    const opts = {};

    return sitesApi
      .getSite(id, opts)
      .then((data) => {
        return data.entry;
      })
      .catch((err) => {
        throw newPristyError("getWorkspace", err);
      });
  }

  getWorkspaceMemberships(id) {
    const opts = {};

    return sitesApi
      .listSiteMemberships(id, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getWorkspaceMemberships", err);
      });
  }

  getWorkspaceMembershipsWithOpts(id, opts) {
    return sitesApi.listSiteMemberships(id, opts).catch((err) => {
      throw newPristyError("getWorkspaceMemberships", err);
    });
  }

  getWorkspaceGroupsWithOpts(id, opts) {
    return sitesApi
      .listSiteGroups(id, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getWorkspaceGroups", err);
      });
  }

  getWorkspaceGroups(id) {
    const opts = {};

    return sitesApi
      .listSiteGroups(id, opts)
      .then((data) => {
        return data.list.entries.map((data) => data.entry);
      })
      .catch((err) => {
        throw newPristyError("getWorkspaceGroups", err);
      });
  }

  createWorkspace(id, title, descr, visibility) {
    const opts = {
      id: id,
      title: title,
      description: descr,
      visibility: visibility,
    };

    return sitesApi.createSite(opts).catch((err) => {
      throw newPristyError("createWorkspace", err);
    });
  }

  requestJoinWorkspace(personId, siteId) {
    const siteMemberShipRequestBody = {
      id: siteId,
    };
    const opts = {};

    return sitesApi
      .createSiteMembershipRequestForPerson(
        personId,
        siteMemberShipRequestBody,
        opts,
      )
      .catch((err) => {
        throw newPristyError("requestJoinWorkspace", err);
      });
  }

  addWorkspaceUser(siteId, userId, role) {
    const memberShipBody = {
      role: role,
      id: userId,
    };
    const opts = {};

    return sitesApi
      .createSiteMembership(siteId, memberShipBody, opts)
      .catch((err) => {
        throw newPristyError("addWorkspaceUser", err);
      });
  }

  addWorkspaceGroup(siteId, groupId, role) {
    const groupBody = {
      role: role,
      id: groupId,
    };
    const opts = {};

    return sitesApi
      .createSiteGroupMembership(siteId, groupBody, opts)
      .catch((err) => {
        throw newPristyError("addWorkspaceGroup", err);
      });
  }

  updateWorkspaceUserRole(siteId, userId, newRole) {
    const siteMembershipBody = {
      role: newRole,
    };
    const opts = {};

    return sitesApi
      .updateSiteMembership(siteId, userId, siteMembershipBody, opts)
      .catch((err) => {
        throw newPristyError("updateWorkspaceUserRole", err);
      });
  }

  updateWorkspaceGroupRole(siteId, groupId, newRole) {
    const siteGroupBody = {
      role: newRole,
    };
    const opts = {};

    return sitesApi
      .updateSiteGroupMembership(siteId, groupId, siteGroupBody, opts)
      .catch((err) => {
        throw newPristyError("updateWorkspaceGroupRole", err);
      });
  }

  deleteWorkspaceUser(siteId, userId) {
    return sitesApi.deleteSiteMembership(siteId, userId).catch((err) => {
      throw newPristyError("deleteWorkspaceUser", err);
    });
  }

  deleteWorkspaceGroup(siteId, groupId) {
    return sitesApi.deleteSiteGroupMembership(siteId, groupId).catch((err) => {
      throw newPristyError("deleteWorkspaceGroup", err);
    });
  }

  updateActeWorkspaceType(id, typeEspace) {
    const opts = {
      properties: {
        "pm:typeEspace": typeEspace,
      },
      aspectNames: [
        "cm:tagscope",
        "pm:specializeEspace",
        "cm:titled",
        "cm:auditable",
        "am:isPublic",
      ],
    };
    return nodesApi.updateNode(id, opts).catch((err) => {
      throw newPristyError("updateWorkspaceType", err);
    });
  }

  updateWorkspaceType(id, typeEspace) {
    const opts = {
      properties: {
        "pm:typeEspace": typeEspace,
      },
    };

    return nodesApi.updateNode(id, opts).catch((err) => {
      throw newPristyError("updateWorkspaceType", err);
    });
  }

  async getAllUsers() {
    return peopleApi
      .listPeople()
      .then((response) => {
        return response.list.entries.map((personEntry) => {
          let person = { ...personEntry.entry };
          person.label = `${person.firstName} ${person.lastName}`;
          person.group = false;
          return person;
        });
      })
      .catch((err) => {
        throw newPristyError("getAllGroups", err);
      });
  }

  async getAllGroups() {
    return groupsApi
      .listGroups()
      .then((response) => {
        return response.list.entries.map((groupEntry) => {
          let group = groupEntry.entry;
          return {
            firstname: "",
            lastname: "",
            email: "",
            id: group.id,
            label: group.displayName,
            group: true,
          };
        });
      })
      .then((response) => {
        const excludedGroups = ["site_", "SITE_", "ALFRESCO_", "EMAIL_"];
        return response.filter(
          (group) =>
            !excludedGroups.some((word) => group.label.startsWith(word)),
        );
      })
      .catch((err) => {
        throw newPristyError("listGroups", err);
      });
  }

  deleteWorkspace(siteId) {
    const opts = {};

    return sitesApi.deleteSite(siteId, opts).catch((err) => {
      throw newPristyError("deleteWorkspace", err);
    });
  }

  updateWorkspace(id, opts) {
    return sitesApi.updateSite(id, opts).catch((err) => {
      throw newPristyError("updateWorkspace", err);
    });
  }
}

export default new WorkspaceService();
