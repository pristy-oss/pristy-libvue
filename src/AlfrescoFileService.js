import {
  contentApi,
  uploadApi,
  renditionsApi,
  versionsApi,
} from "./AlfrescoApi";
import errorService from "./ErrorService";
import retry from "async-retry";

class AlfrescoFileService {
  getThumbnailUrl(nodeId) {
    return contentApi.getDocumentThumbnailUrl(nodeId);
  }

  getPreviewUrl(nodeId) {
    return contentApi.getDocumentPreviewUrl(nodeId);
  }

  getContentUrl(nodeId) {
    return contentApi.getContentUrl(nodeId);
  }

  getVersionContentUrl(nodeId, versionId) {
    return contentApi.getVersionContentUrl(nodeId, versionId);
  }

  uploadFile(nodeId, opts, file) {
    return uploadApi
      .uploadFile(file, null, nodeId, null, opts)
      .on("progress", (progress) => {
        // console.log("Total :" + progress.total);
        // console.log("Loaded :" + progress.loaded);
        console.log("Percent :" + progress.percent);
      })
      .then((data) => {
        this.createAllRendition(data.entry.id);
        return data;
      })
      .catch((error) => {
        errorService.uploadFileError(error, file.name);
      });
  }

  createRendition(nodeId, rendition) {
    return renditionsApi
      .createRendition(nodeId, {
        id: rendition,
      })
      .catch((error) => {
        if (error.status === 400) {
          let err = JSON.parse(error.message);
          console.warn(err.error?.briefSummary);
        } else {
          console.error(error);
        }
      });
  }

  createAllRendition(nodeId) {
    const promises = [];
    promises.push(
      renditionsApi.createRendition(nodeId, {
        id: "doclib",
      }),
    );
    promises.push(
      renditionsApi.createRendition(nodeId, {
        id: "imgpreview",
      }),
    );
    return Promise.allSettled(promises);
  }

  createVersionRendition(nodeId, versionId, rendition) {
    return versionsApi.createVersionRendition(nodeId, versionId, {
      id: rendition,
    });
  }

  getVersionRendition(nodeId, versionId, renditionId) {
    return versionsApi.getVersionRendition(nodeId, versionId, renditionId);
  }

  /**
   * Get rendition of the selected node. The method will retry 5 times, waiting for rendition to be created
   * @param nodeId
   * @param rendition default to pdf"
   * @returns {Promise<unknown>}
   */
  getRenditionUrl(nodeId, rendition = "pdf") {
    return retry(
      async () => {
        // if anything throws, we retry
        return renditionsApi
          .getRendition(nodeId, rendition)
          .then((pdfRendition) => {
            if (pdfRendition.entry.status === "NOT_CREATED") {
              renditionsApi.createRendition(nodeId, {
                id: rendition,
              });
              // retry
              throw new Error("NOT_CREATED");
            } else if (pdfRendition.entry.status === "CREATED") {
              return contentApi.getRenditionUrl(nodeId, rendition);
            } else {
              throw new Error(pdfRendition.entry.status);
            }
          })
          .catch((error) => {
            console.error("createRendition", error);
            if (error.status === 409) {
              return contentApi.getRenditionUrl(nodeId, rendition);
            } else {
              throw error;
            }
          });
      },
      {
        retries: 5,
        onRetry: (error) => {
          console.log("Retry getRenditionUrl", error?.message);
        },
      },
    );
  }

  /**
   * Get rendition of the selected version. The method will retry 5 times, waiting for rendition to be created
   * @param nodeId
   * @param versionId
   * @param rendition default to pdf"
   * @returns {*}
   */
  getVersionRenditionUrl(nodeId, versionId, rendition = "pdf") {
    return retry(
      async () => {
        // if anything throws, we retry
        return versionsApi
          .getVersionRendition(nodeId, versionId, rendition)
          .then((pdfRendition) => {
            if (pdfRendition.entry.status === "NOT_CREATED") {
              versionsApi.createVersionRendition(nodeId, versionId, {
                id: rendition,
              });
              // retry
              throw new Error("NOT_CREATED");
            } else if (pdfRendition.entry.status === "CREATED") {
              return contentApi.getVersionRenditionUrl(
                nodeId,
                versionId,
                rendition,
              );
            } else {
              throw new Error(pdfRendition.entry.status);
            }
          })
          .catch((error) => {
            console.error("createRendition", error);
            if (error.status === 409) {
              return contentApi.getVersionRenditionUrl(
                nodeId,
                versionId,
                rendition,
              );
            } else {
              throw error;
            }
          });
      },
      {
        retries: 5,
        onRetry: (error) => {
          console.log("Retry getVersionRenditionUrl", error?.message);
        },
      },
    );
  }
}

export default new AlfrescoFileService();
