
0.33.1 / 2025-03-02
==================

- Add Matomo configuration

0.33.0 / 2025-02-26
==================

- Prepare for 404 in userHome navigation
- User home, adapt navigation store and add getHomeFolderFromUser"
- Prepare for using User Home

0.32.3 / 2025-02-10
==================

- Correct lockedNode error handling
- Correct default element per page before override
- FIX: Error message when updating a locked node
- FIX: update site does not use site api
- FIX: retrieve favorite

0.32.2 / 2025-01-28
==================

- Add isInConslutation in navigation store

0.32.1 / 2025-01-27
==================

- Add ROW_PER_PAGE_OPTIONS in config for pagination
- Add DEFAULT_ELEMENT_PER_PAGE in config for pagination
- FIX: handle workflow requests with idUser containing certain characters
- Update dependencies

0.32.0 / 2025-01-15
==================

- Complete workflow service with espaces function
- Update package @alfresco/js-api
- Add workflow service
- Keycloack Plugin based on keycloak-js

0.31.0 / 2024-12-18
==================

- Add getChildrenWithOpts + loadWithOpts for fileList pagination
- Correct people service for pagination

0.30.7 / 2024-12-09
==================

- Add in config reset password rules

0.30.6 / 2024-10-14
==================

- Add nature document and plan de classement ASE
- Filter favorite site for menu

0.30.5 / 2024-10-01
==================

- Use opt in list members and list groups function

0.30.4 / 2024-10-01
==================

- Correct plan de classement

0.30.3 / 2024-09-20
==================

- Use parametrable sort in searchStore

0.30.2 / 2024-09-16
==================

- Add social menu variables in config

0.30.1 / 2024-09-11
==================

- part plan de classement AM and AF

0.30.0 / 2024-09-05
==================

- Add OIDC_RESPONSE_TYPE parameter for Auze-AD
- update dependencies
  - axios 1.7.7
  - pinia 2.2.2
  - alfresco/js-api: 7.2.0

0.29.2 / 2024-08-28
==================

- Add NATURE_DOCUMENT_AMAF to config
- Add PLAN_CLASSMENT_AMAF to config

0.29.1 / 2024-07-02
==================

- add getDirectChildren function

0.29.0 / 2024-06-03
==================

- Add action view and view_comment for Collabora Online
- fix theme loading (remove promise)
- add dark variable in themeStore
- maxItems set to 1k
- Remove useless ErrorService functions
- Add description of function loadOnActes in navigationStore

0.28.0 / 2024-04-03
==================

- Add MENU_MARCHES to config store
- use navigationStore on pristy-actes
- Change primevue version to 3.50.0

0.27.2 / 2024-03-14
==================

- Update primevue version + update config

0.27.1 / 2024-02-01
==================

- Add export groupsApi

0.27.0 / 2024-01-30
==================

- update vite - CVE-2024-23331
- Add scale in useThemeStore 
- Fix: undefined allowableOperations in canUpload assignment in navigationStore
- Add missing homeFolder uuid in user info
- Update dependencies
- use nodejs 20.9

0.26.1 / 2024-01-25
==================

- Add comment on Version

0.26.0 / 2024-01-23
==================

- Add themeStore

0.25.2 / 2024-01-10
==================

- Correct canUpload on navigation

0.25.1 / 2024-01-03
==================

- Correct workspace service for permissions

0.25.0 / 2023-11-25
==================

- remove old breadcumb service
- fix log message, error may have no status
- SearchStore: rename filterQuery to query. This break API

0.24.1 / 2023-11-24
==================

- AlfrescoNodeService: compute children of documentLibrary with nodesApi
- NavigationStore: reset pagination at each load

0.24.0 / 2023-11-22
==================

- getChildren: add pagination information. This break API
- getChildren: default maxItems to 1k
- Move connectKeycloak function in user store

0.23.5 / 2023-11-08
==================

- If gerPerson successs, isLoggedIn=true
- UserStore: add comment on public method

0.23.4 / 2023-11-06
==================

- On searching, correct removing site filter removes folder filter
- On searching, removing site filter removes folder filter

0.23.3 / 2023-11-02
==================

- UserStore: correct message when bad login
- UserStore: catch error when disconnected

0.23.2 / 2023-11-02
==================

- Code quality: correct favorite node icon

0.23.1 / 2023-10-30
==================

- Correct condition on adding field in searching

0.23.0 / 2023-10-25
==================

- UserStore: rewrite api
    - for keycloak auth, renew token every 60s
    - replace isLoggedIn method by validateSession, that only check authent
    - isLoggedIn is now a boolean
    - add getAlfTicket() that log to Alfresco with Bearer token and set alf_token
    - remote me() getters, not need
- Add missing translation for error 401

0.22.5 / 2023-10-18
==================

- Lemonldap: port change from keycloak config
- Keycloak: No redirectUri to use location.href
- Keycloak: wait alfresco to login avec keycloak is ready

0.22.4 / 2023-10-18
==================

- ConfigStore: load keycloak.json from APP_ROOT

0.22.3 / 2023-10-15
==================

- FavoriteService: add call to menuStore
- MenuService: add warn if missing path in favorite node
- MenuStore: prevent error if undefined parameters
- NavigationStore: can force loading data
- add npm run build:dev

0.22.2 / 2023-10-14
==================

- NavigationStore: fix import

0.22.1 / 2023-10-14
==================

- Missing navigation store export
- update release script

0.22.0 / 2023-10-14
==================

- MenuService: use remix icon
- New navigation store

0.21.1 / 2023-10-12
===================

- UserPref: listView become default
- redefine selection store
- Set libvue as type=module

0.21.0 / 2023-10-09
===================

- Breaking change on config.init

0.20.5 / 2023-09-30
===================

- IconService: Use isFile and isFolder instead of nodeType

0.20.4 / 2023-09-28
===================

- SearchStore: filter facets to match API

0.20.3 / 2023-09-28
===================

- fix filter bucket.query
- SearchStore: filterQueries map list to match API

0.20.2 / 2023-09-27
===================

- SearchStore: fields and filterQueries can store label

0.20.1 / 2023-09-26
===================

- update release script
- SearchStore: need new list when reset
  
*Update Dependencies**

- update axios 1.5.1
- update pinia 2.1.6
- update primevue 3.35.0
- update vue-i18n 9.4.1
- update vite 3.2.7

0.19.3 / 2023-09-26
===================

- WorspaceService, getAllUsers will return all md
- Rewrite Search Store
- copy SelectionStore from pristy-espaces
- copy UserPreferencesStore from pristy-espaces
- copy searchStore from pristy-espaces

0.19.2 / 2023-09-19
===================

- Add Contraints Config

0.19.1 / 2023-09-17
===================

- add ASPECTS map config

0.18.3 / 2023-09-14
===================

- release.sh: bug on tag release
- Throw error on shared link call
- create rendition: catch error 400
- Change default preview url
- i18n: silent warn

0.18.2 / 2023-09-08
===================

- add Sentry parameter
- New release script

0.18.1 / 2023-08-08
===================

- sharelink function for consultation

0.18.0 / 2023-07-13
===================

- Add missing Copyright
- Simplify ErrorService

## [0.18.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.17.4...0.18.0) (2023-07-13)

**Enhancements:**

- Simplify ErrorService

## [0.17.4](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.17.3...0.17.4) (2023-07-10)

- Add SHOW_ACA_LOGIN_LINK config

## [0.17.3](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.17.2...0.17.3) (2023-07-03)

- Add ACA_LINK config
- Update locales

## [0.17.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.17.1...0.17.2) (2023-06-06)

- Add OIDC_CUSTOM config

## [0.17.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.16.5...0.17.1) (2023-05-18)

**Enhancements:**

- NodeService: add checkVersion, that create an initale version if needed
- AlfrescoFileService: add getRenditionUrl and getVersionRenditionUrl
- AlfrescoFileService: split createAllRendition ..

**Update Dependencies**

- add async-retry 1.3.3
- update @alfresco/js-api 5.5.0
- update axios 1.4.0
- update pinia 2.0.36
- update primevue 3.28.0

## [0.16.5](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.16.4...0.16.5) (2023-05-09)

**Enhancements:**

- acte: redirect to default page if authentication not succeed

## [0.16.4](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.16.3...0.16.4) (2023-05-09)

**Enhancements:**

- add OIDC default config
- config: add pimevue config
- geter oidc with lemonldap config

## [0.16.3](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.16.2...0.16.3) (2023-04-12)

**New Features and Enhancements:**

- AlfrescoNodeService add getNodeContent
- User store, add me getter

## [0.16.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.16.0...0.16.2) (2023-04-07)

**Fixed bugs:**

- Fix login issue with new loading api strategy

## [0.16.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.15.0...0.16.0) (2023-04-05)

**New Features and Enhancements:**

- rewrite how Alfresco API are loaded, this break code

## [0.15.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.14.4...0.15.0) (2023-03-30)

**New Features and Enhancements:**

* Create people service
* Add missing Alfresco API
* Add getContentVersion function in AlfrescoNodeService
* create and get renditions for versions
* add content url for rendition and versions

**Update Dependencies**

* update primevue 3.26.1


## [0.14.4](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.14.1...0.14.4) (2023-03-21)

**Fixed bugs:**

- correctly loading collabora store

## [0.14.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.13.3...0.14.1) (2023-03-20)

**New Features and Enhancements:**

- Collabora Discovery: use a store instead of a service

## [0.13.3](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.13.2...0.13.3) (2023-03-20)

**New Features and Enhancements:**

- Config Collabora Online to load hosting descovery file
- Change default to non-usable default

## [0.13.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.13.1...0.13.2) (2023-03-16)

**New Features and Enhancements:**

- Add keycloak host and realm in config

**Fixed bugs:**

- Return uploadFile response correctly

## [0.13.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.13.0...0.13.1) (2023-03-15)

**New Features and Enhancements:**

- Delete unused functions
- Add revert function for versions

## [0.13.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.12.2...0.13.0) (2023-03-14)

**New Features and Enhancements:**

- Add functions to getItemHref for a node and add canOpenWithCollabora
- loadExtensionsXML using axios

**Update Dependencies**

- Update pinia 2.0.33
- Update primevue 3.25.0

## [0.12.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.12.1...0.12.2) (2023-03-05)

- export singleton alfrescoNodeService
- Typo erroService in AlfrescoFileService
- Rename singleton instance to start with lowercase

## [0.12.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.12.0...0.12.1) (2023-03-05)

- User singleton pattern for services
- correcting errorService loading

## [0.12.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.11.1...0.12.0) (2023-03-05)

**New Features and Enhancements:**

- Add request to join a workspace
- Replace AppService with config store


**Update Dependencies**

- Update alfresco/js-api 5.4.0

## [0.11.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.11.0...0.11.1) (2023-03-01)

**New Features and Enhancements:**

- Create Service to get the app Config
- Add menu variables for pristy portail

- **Fixed bugs:**

- remove ticket-ECM from localStorage to force logout
- Add pristy-actes default values

## [0.11.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.10.0...0.11.0) (2023-02-28)

**New Features and Enhancements:**

- Add favorites to MenuStore
- Add version history display management

- **Fixed bugs:**

- Correction syntax locales JSON files
- Correction version comment

**Update Dependencies**

- Update alfresco/js-api 5.2.0
- Update axios 1.3.4
- Update pinia 2.0.32
- Update primevue 3.23.0

## [0.10.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.9.2...0.10.0) (2023-02-20)

**New Features and Enhancements:**

- Add error 507 handling for quotas
- Add error Add error management on comment creation on comment creation
- Error management for 502 504 addNodeComment
- Add management shareLink (api, function, error)

## [0.9.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.9.1...0.9.2) (2023-02-14)

**New Features and Enhancements:**

- Add commentsApi
- creation function createComment in AlfrescoNodeService
- In breadcrumb path, display title of workspace instead of id

## [0.9.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.9.0...0.9.1) (2023-02-09)

- Add path and isLocked in the include of the function getChildren in AlfrescoNodeService

## [0.9.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.8.1...0.9.0) (2023-01-31)

**New Features and Enhancements:**

- Change catchLogin error in function isLoggedIn
- Add BreadcrumbService to library

**Fixed bugs:**

- change error to catch initial error object

## [0.8.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.8.0...0.8.1) (2023-01-24)

- Add groupsApi from alfresco/js-api 
- Create getAllGroups function
- Add group management functions
- Translation and codeQuality to rename variables & functions

## [0.8.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.7.0...0.8.0) (2023-01-19)

**New Features and Enhancements:**

- Add Keycloak / OIDC Authentication
- Better error management when Alfresco is down

**Update Dependencies**

- Update vite 3.2.5
- Update axios 1.2.2
- Update alfresco/js-api 5.2.0
- Update primevue 3.22.1

## [0.7.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.6.2...0.7.0) (2023-01-04)

- Retrieve user language from localStorage or window language
- Preparing for easy inclusion of new languages
- Add property "path" for getNode and getParents functions

## [0.6.2](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.6.1...0.6.2) (2022-12-19)

- Fix bug with upload error 409

## [0.6.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.6.0...0.6.1) (2022-12-08)

- Added icone service with file identification
- English translation

**Update Dependencies**

- Update @alfresco/js-api 5.1.0                                   |

## [0.6.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.5.2...0.6.0) (2022-12-02)

- Adding file icons

## [0.5.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.5.0...0.5.1) (2022-11-25)

- Setting up translation on ErrorService

## [0.5.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.4.1...0.5.0) (2022-11-23)

- Setting up vitest
- Workspace Service error handling
- Add options for updateContentNode

## [0.4.1](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.4.0...0.4.1) (2022-11-10)

- Error management for 502/504
- Add workspace to favorites

## [0.4.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.3.3...0.4.0) (2022-10-31)

**New Features and Enhancements:**

- Add CollaboraAccessService and CollaboraExtensionService

## [0.3.3](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.3.0...0.3.3) (2022-10-20)

**New Features and Enhancements:**

- Add FavoriteService

## [0.3.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.7...0.3.0) (2022-10-14)

- Build with vite and linter 

**Update Dependencies**

- Update @alfresco/js-api 5.0.0

## [0.2.7](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.6...0.2.7) (2022-10-11)

- Fix import of TrashCanApi

## [0.2.6](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.5...0.2.6) (2022-10-07)

- Update wording

## [0.2.5](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.4...0.2.5) (2022-10-05)

- Update Readme and licence in package.json

## [0.2.4](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.3...0.2.4) (2022-10-04)

- Adding the publication of an act

## [0.2.3](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.2.0...0.2.3) (2022-09-22)

- Add workspaceService

## [0.2.0](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.1.4...0.2.0) (2022-09-15)

- Add copy and move nodes functions, Error management corrections

## [0.1.4](https://gitlab.com/pristy-oss/pristy-libvue/-/compare/0.1.3...0.1.4) (2022-08-30)

- First stable release

