# Pristy Libvue

Code factoring for Pristy

## Developing with Pristy-libvue

Add `@pristy/pristy-libvue` package.


Then call the available classes where necessary. Be careful, there is no default class, you must use {} in the import.

```vue
import { AlfrescoFileService } from "@pristy/pristy-libvue"
```


## Dev locally

1. In prity-libvue

```
npm link
```

2. In another pristy application

```
npm link <local directory off pristy-libvue>
```
The library needs to be built by vite to be usable locally.
``` 
npm run build:library
```

The pristy-libvue package of the package.json will be overloaded by the local library.
