import { describe, expect, test } from "vitest";
import { newPristyError } from "/src/ErrorService";
import i18n from "/src/i18n/index";

const { t } = i18n.global;

describe("ErrorService", () => {
  const error = {
    status: 404,
    message: "Item not found",
  };
  test("404 on different functions", () => {
    expect(() => {
      throw newPristyError("moveNode", error);
    }).toThrowError(t("error.moveNode.404"));
    expect(() => {
      throw newPristyError("copyNode", error);
    }).toThrowError(t("error.copyNode.404"));
    expect(() => {
      throw newPristyError("uploadFile", error);
    }).toThrowError(t("error.uploadFile.404"));
  });
});
