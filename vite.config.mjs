import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vite";
import { resolve } from "path";
import eslintPlugin from "vite-plugin-eslint";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [eslintPlugin(), vue()],
  test: {
    environment: "jsdom",
    coverage: {
      reporter: ["text", "json", "html"],
    },
  },
  build: {
    lib: {
      entry: resolve(__dirname, "src/index.js"),
      name: "pristy-libvue",
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [
        "vue",
        "@alfresco/js-api",
        "primevue",
        "axios",
        "pinia",
        "vue-i18n",
        "keycloak-js",
      ],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: "Vue",
          "@alfresco/js-api": "AlfrescoJsApi",
          primevue: "PrimeVue",
          "vue-i18n": "vueI18n",
          axios: "axios",
          pinia: "pinia",
        },
      },
    },
  },
});
