import { visualizer } from "rollup-plugin-visualizer";
import vue from "@vitejs/plugin-vue";

import { defineConfig } from "vite";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    visualizer({
      open: true,
      title: 'Pristy Library Bundle Visualizer',
    }),
  ],
  build: {
    lib: {
      entry: resolve(__dirname, "src/index.js"),
      name: 'PristyLibVue',
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ["vue", "@alfresco/js-api", "primevue"],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: "Vue",
          "@alfresco/js-api": "AlfrescoJsApi",
          primevue: "PrimeVue",
        },
      },
    },
  },
})
